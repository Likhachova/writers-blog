const AllINFOARTICLES = [];
const AllINFONEWS = [];
var nodeInDomNews;
var nodeInDomArticle;

//show all information from local storage
window.onload = function () {
    var tellAbout =  document.getElementById('tellabout');
    var uploadImage = document.getElementById('upload_image');
    var addArticle = document.getElementById('add_article');
    var addNews =  document.getElementById('add_news');
    if(tellAbout){
        tellAbout.addEventListener("click", saveAbout());
    }
    if(uploadImage){
        uploadImage.addEventListener("click", savePicture());
    }
    if(addArticle){
        addArticle.addEventListener("click", savePicture());
    }
    if(addNews){
        addNews.addEventListener("click", savePicture());
    }
    displayName();
    displayPhoto();
    hideMenus();
}

//function to redirect to blog
function redirectBlog(){
    localStorage.setItem("AllINFOARTICLES", JSON.stringify(AllINFOARTICLES));
    localStorage.setItem("AllINFONEWS", JSON.stringify(AllINFONEWS));
    showBlog();
}

//function to show all info in blog
function showBlog(){
    const storedArticles = JSON.parse(localStorage.getItem("AllINFOARTICLES"));
    const storedNews = JSON.parse(localStorage.getItem("AllINFONEWS"));
    console.log(typeof storedArticles);
    storedArticles.map( item => item.render() );
    storedNews.map( item => item.render() );
}

//function to store user picture 
function savePicture() {
    hideMenus();
    document.getElementById('menu-picture').style.display = "block";
}

//function to store user history  
function saveAbout() {
    hideMenus();
    document.getElementById('menu-history').style.display = "block";
    let historyButton = document.getElementById('history-button');
    historyButton.addEventListener("click", function () {
        if (document.getElementById('history').value) {
            idHistory = document.getElementById('history').value;
            localStorage.setItem("user.history", idHistory);
        }
    });
}

//function to store user article
function saveArticle() {
    hideMenus();
    document.getElementById('menu-article').style.display = "block";
    let articleButton = document.getElementById('article-button');
    articleButton.addEventListener("click", function () {
        if (document.getElementById('article-title').value && document.getElementById('article-description').value) {
            let articleTitle = document.getElementById('article-title').value;
            let articleDescription = document.getElementById('article-description').value;
            new Article(articleTitle, articleDescription);
            localStorage.setItem("user.articleTitle", articleTitle);
            localStorage.setItem("user.articleDescription", articleDescription);
            hideMenus();
            success();
        }
    });
}

//function to store user news
function saveNews() { 
    hideMenus();
    document.getElementById('menu-new').style.display = "block";
    let newsButton = document.getElementById('news-button');
    newsButton.addEventListener("click", function () {
        if (document.getElementById('news-title').value && document.getElementById('news-description').value) {
            newsTitle = document.getElementById('news-title').value;
            newsDescription = document.getElementById('news-description').value;
            new News(newsTitle, newsDescription);
            localStorage.setItem("user.newsTitle", newsTitle);
            localStorage.setItem("user.newsDescription", newsDescription);
            hideMenus();
            success();
        }
    });
}

//function to upload new picture
function uploadPicture() {
    var outImage = "subtitle-cabinet";
    console.log("gjhvu");
    document.getElementById('picField').onchange = function (evt) {
        var tgt = evt.target || window.event.srcElement,
            files = tgt.files;
        // FileReader support
        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function () {
                document.getElementById(outImage).src = fr.result;
            }
            fr.readAsDataURL(files[0]);
        }
    }
    bannerImage = document.getElementById('subtitle-cabinet');
    imgData = getBase64Image(bannerImage);
    localStorage.setItem("user.picture", imgData);
}

//save photo as Base64
function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

//retrieve photo from storage
function displayPhoto() {
    var dataImage = localStorage.getItem('user.picture');
    if (dataImage) {
    bannerImg = document.getElementById('subtitle-cabinet');
    bannerImg.src = "data:image/png;base64," + dataImage;
    }
}

//retrieve photo from storage
function displayName() {
    var localUserName = localStorage.getItem("user.name");
    if (localUserName) {
        if(document.getElementById('title-user')){
            document.getElementById('title-user').innerHTML = localUserName;
        }
  
    }
}

// function to read image
function onFileSelected(event) {
    var selectedFile = event.target.files[0];
    var reader = new FileReader();
    var imgtag = document.getElementById("subtitle-cabinet");
    reader.onload = function (event) {
        imgtag.src = event.target.result;
    };
    reader.readAsDataURL(selectedFile);
}

// function to display user information
function showAbout() {
    hideMenus();
    document.getElementById('menu-history-description').style.display = "block";
    if (localStorage.getItem('user.history')) {
        document.getElementById('your-history').innerHTML = localStorage.getItem('user.history');
    }
}

// function to display all articles and news
function showArticle() {
    hideMenus();
    document.getElementById('menu-related-articles').style.display = "block";
    AllINFOARTICLES.map( item => item.render() );
    AllINFONEWS.map( item => item.render() );
  
}

//function to switch on facebook messenger, site needs to be hosted to switch it on
function showMessenger() {
    hideMenus();
    //document.getElementById('facebook-messenger-chat').style.display="block";
}

//function to display success
function success(){
    document.getElementById('menu-success').style.display = "block";
}

//function to hide menu
function hideMenus() {
    if(document.getElementById('menu-history')){ document.getElementById('menu-history').style.display = "none";}
    if(document.getElementById('menu-new')){   document.getElementById('menu-new').style.display = "none";}
    if(document.getElementById('menu-article')){ document.getElementById('menu-article').style.display = "none";}
    if(document.getElementById('menu-picture')){document.getElementById('menu-picture').style.display = "none";}
    if(document.getElementById('menu-history-description')){ document.getElementById('menu-history-description').style.display = "none";}
    if(document.getElementById('menu-related-articles')){document.getElementById('menu-related-articles').style.display = "none";}
    if(document.getElementById('menu-success')){document.getElementById('menu-success').style.display = "none";}
}

//function to store user name and password
function store() {
    let username = document.getElementById('username').value;
    let userpassword = document.getElementById('password').value;
    if (username == "" || userpassword == "") {
        document.getElementById('errorMessage').innerHTML = "Field can not be empty";
    } else {
        new User(username, userpassword);
        localStorage.setItem("user.name", username);
        localStorage.setItem("user.password", userpassword);
        window.location.href = "cabinet.html";
    }
}

//function to sign in
function login() {
   let username = document.getElementById('username').value;
   let userpassword = document.getElementById('password').value;
    if ((username == localStorage.getItem(' user.name')) && (userpassword == localStorage.getItem('user.password'))) {
        document.getElementById('welcomeMessage').innerHTML = "Welcome " + localStorage.getItem('username') + "!";
        window.location.href = "cabinet.html";
    } else {
        document.getElementById('welcomeMessage').innerHTML = "Invalid Log-in!";
    }
}

class User {
    constructor({
        userName,
        userPassword
    }) {
        this.id = Math.random() * 10000;
        this.name = userName;
        this.password = userPassword;
    }
    
}

class Article {
    constructor({
        name,
        description
    }) {
        this.id = Math.random() * 10000;
        this.title = name;
        this.text = description;
        this.likes = 0;
        this.dislikes = 0;
        this.Like = this.Like.bind(this);
        this.Dislike = this.Dislike.bind(this);
        AllINFOARTICLES.push(this);
    }

    Like() {
        this.likes++;
        this.render();
    }

    Dislike() {
        this.dislikes++;
        this.render();
    }

    render() {
        nodeInDomArticle = document.querySelector(`.post-section[data-id="${this.id}`);
        const target = document.getElementById('articles');
        let node;

        if (nodeInDomArticle !== null) {
            node = nodeInDomArticle;

        } else {
            node = document.createElement('div');
            node.className = "post-between";
            
        }

        node.innerHTML = `
                    <div class="post-section" data-id="${this.id}">
                        <h2 class="post-title">${this.title}</h2>
                        <p class="post-text">
                            ${this.text}
                        </p>
                        <div class="post-section-btn" >
                        <button class="like_btn post-btn"> <i class="far fa-thumbs-up"></i>Like ${this.likes} </button>
                        <button class="dislike_btn post-btn"> <i class="far fa-thumbs-down"></i>Dislike ${this.dislikes} </button>
                        </div>
                    </div>
                `;

        let like_btn = node.querySelector('.like_btn');
        let dislike_btn = node.querySelector('.dislike_btn');
        like_btn.onclick = this.Like;
        dislike_btn.onclick = this.Dislike;
    
        if (nodeInDomArticle === null) {
            console.log("we here to like");
            target.appendChild(node);
        }

    }

}


class News extends Article {
    constructor(title, text) {
        super(title, text);
        AllINFONEWS.push(this);
    }
    Like() {
        this.likes++;
        this.render();
    }

    Dislike() {
        this.dislikes++;
        this.render();
    }
    render() {
        nodeInDomNews = document.querySelector(`.post-section[data-id="${this.id}`);
        const target = document.getElementById('news');

        let node;
        if (nodeInDomNews !== null) {
            node = nodeInDomNews;
        } else {
            node = document.createElement('div');
            node.className = "post-between";
        }

        node.innerHTML = `
        <div class="post-section" data-id="${this.id}">
        <h2 class="post-title">${this.title}</h2>
        <p class="post-text">
            ${this.text}
        </p>
        <div class="post-section-btn" >
        <button class="like_btn post-btn"> <i class="far fa-thumbs-up"></i>Like ${this.likes} </button>
        <button class="dislike_btn post-btn"> <i class="far fa-thumbs-down"></i>Dislike ${this.dislikes} </button>
        </div>
    </div>
                `;

        let like_btn = node.querySelector('.like_btn');
        like_btn.onclick = this.Like;

        let dislike_btn = node.querySelector('.dislike_btn');
        dislike_btn.onclick = this.Dislike;

        if (nodeInDomNews === null) {
            target.appendChild(node);
        }

    }

}

 